# Date and time
date=$(date +"%d/%m/%Y")
time=$(date +"%H:%M")

bat_charge=$(upower --show-info $(upower --enumerate | grep 'BAT') | egrep "percentage" | awk '{print $2}')
bat_state=$(upower --show-info $(upower --enumerate | grep 'BAT') | egrep "state" | awk '{print $2}')

if [bat_state = 'discharging'];
then
  bat_pluggedin='DISCHAR'
else
  bat_pluggedin='FULL'
fi

loadavg_5min=$(cat /proc/loadavg | awk -F ' ' '{print $2}')

# Bar output
echo "| BAT: $bat_charge | $date | $time "
